# Jellyfin Tizen Automatic Builder and Installer

Should support Samsung TV-s with Tizen 2.4-5.0 (Currently 5.0 is tested)

Docker assisted, so it won't make a mess. :)

This is **NOT** an official Jellyfin repository.

## Dependencies

- docker

## Setup

1. Clone/download the [Jellyfin Web](https://github.com/jellyfin/jellyfin-web) repository into a folder
2. Clone/download the [Jellyfin Tizen](https://github.com/jellyfin/jellyfin-tizen) repository into a folder
3. Put your author certificate (author.p12) and your distributor certificate (distributor.p12) into a folder
4. Modify the configuration to match your setup by editing `BuildTizenApp.sh`:
```Bash
JELLYFIN_WEB=`pwd`/jellyfin-web/
JELLYFIN_TIZEN=`pwd`/jellyfin-tizen/
TIZEN_CERTS=`pwd`/certs/ # Need the author.p12 and the distributor.p12 to be in this directory
IP="192.168.0.XY" # The IP of the TV to install
AUTHOR_PASS="YOUR AUTHOR CERT PASSWORD"
DISTRIBUTOR_PASS="YOUR DISTRIBUTOR CERT PASSWORD"
```
5. Turn on your TV
6. [Enable developer mode](https://developer.samsung.com/smarttv/develop/getting-started/using-sdk/tv-device.html#undefined) on TV
7. Accept the [SAMSUNG TV App SDK LICENSE AND TERMS OF USE](https://developer.samsung.com/smarttv/develop/tools/tv-extension/archive.html) with the following settings:
    - Version: 3.1.2
    - OS: Ubuntu
    - 64bit
8. Run the `BuildTizenApp.sh` (The user invoking the script needs to have permission to run docker commands)
9. The Jellyfin app should get installed on your TV if everything is correct

## Acquiring a certificate

This process need to be done only once, so it is not automated.

1. Build the Docker image with Tizen Studio IDE:
```
docker build -f docker/TizenStudio.Dockerfile -t tizen-studio-ide .
```

2.  Enable access to an X Server (XWayland works too): `xhost +`

> WARNING: This is not the recommended way of giving access!

3. Start the certificate manager:
```
docker run --rm -u developer -v /tmp/.X11-unix:/tmp/.X11-unix -v "`pwd`/certs:/home/developer/SamsungCertificates/" tizen-studio-ide ./tizen-studio/tools/certificate-manager/certificate-manager
```
4. [Create a certificate](https://developer.samsung.com/smarttv/develop/getting-started/setting-up-sdk/creating-certificates.html)
5. The certificates should be created in the `certs` directory where the last command ran.
