#!/bin/sh

# For saved logs: ./BuildTizenApp.sh 2>&1 | tee out.log
JELLYFIN_WEB=`pwd`/jellyfin-web/
JELLYFIN_TIZEN=`pwd`/jellyfin-tizen/
TIZEN_CERTS=`pwd`/certs/ # Need the author.p12 and the distributor.p12 to be in this directory
IP="192.168.0.XY" # The IP of the TV to install
AUTHOR_PASS="YOUR AUTHOR CERT PASSWORD"
DISTRIBUTOR_PASS="YOUR DISTRIBUTOR CERT PASSWORD"

# NO EDIT NEEDED FROM THIS POINT
docker build -f docker/NodeJs.Dockerfile -t nodegit .
docker run --rm -v "${JELLYFIN_WEB}:/src/jellyfin-web/" -v "${JELLYFIN_TIZEN}:/src/jellyfin-tizen/" -v "`pwd`/docker/buildTizenPackage.sh:/src/buildTizenPackage.sh" -w /src nodegit ./buildTizenPackage.sh
docker build -f docker/TizenStudioCLI.Dockerfile -t tizen-studio .
docker run --rm -u developer --net=host -v "${JELLYFIN_TIZEN}:/src/jellyfin-tizen" -v "${TIZEN_CERTS}:/src/certs/" -v "`pwd`/docker/deployPackage.sh:/src/deployPackage.sh" -w /src tizen-studio ./deployPackage.sh $AUTHOR_PASS $DISTRIBUTOR_PASS $IP
