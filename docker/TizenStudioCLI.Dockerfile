FROM ubuntu:20.04

# These are the IDs that will be used by the container. They MUST match the host UID/GID
# If these are not the same as your host user's IDs, please change them
ARG UID=1000
ARG GID=1000

# The non-root user that will be used to run Tizen Studio
ARG T_USER=developer

# Add the non-root user to the system
RUN groupadd -g ${GID} -o ${T_USER} && \
	useradd -m -o -u ${UID} -g ${GID} -s /bin/bash ${T_USER} && \
	mkdir -p /run/user/$UID && \
	chown ${UID}:${GID} /run/user/${UID}


RUN \
  apt-get update && \
  apt-get install -y --no-install-recommends \
  wget \
  # For Tizen Studio:
  zip pciutils gettext rpm2cpio cpio make \
  # For GUI:
#  libgtk-3-0 libxtst6 libgtk2.0-0 openjdk-8-jdk \
  # For the 3.1.2 SDK:
  sudo \
  # For emulator:
#   software-properties-common \
#   && \
#   add-apt-repository ppa:linuxuprising/libpng12 -y && \
#   apt-get update && \
#   apt-get install libxcb-icccm4 libxcb-xfixes0 libcurl3-gnutls libxcb-render-util0 libpng12-0 libxcb-image0 acl libsdl1.2debian libv4l-0 libxcb-randr0 bridge-utils openvpn \
  && \
  # Clear the apt cache to reduce image size
  apt-get clean && rm -rf /var/lib/apt/lists/* \
  && \
  # Allow sudo for the simple user without password:
  echo "${T_USER} ALL=(ALL) NOPASSWD: ALL\n" >> /etc/sudoers
  
# Clear the apt cache to reduce image size
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Switch to the non-root user to run the installer
USER ${T_USER}
WORKDIR /home/${T_USER}

# More available at: https://developer.samsung.com/smarttv/develop/tools/tv-extension/archive.html
# The 3.1.2 Extension includes the 2.4 Tizen SDK
RUN wget http://sdf.samsungcloudcdn.com/Public/UwBWAEMAMAAwADAAMAAyAA==/releases/Tizen/stv_extensions_public_local_img/samsung_tizen_studio_tv_sdk_3_1_2_extension_20171120120009287/TIZEN-SDK-IMG_sync_20171120120009287_ubuntu-64.zip \
    -O tv_sdk_3.1.2_extension.zip

# Set up the required Tizen Studio version
ARG TIZEN_STUDIO_VERSION=4.1

RUN wget http://download.tizen.org/sdk/Installer/tizen-studio_${TIZEN_STUDIO_VERSION}/web-cli_Tizen_Studio_${TIZEN_STUDIO_VERSION}_ubuntu-64.bin \
  -O tizenstudio_installer.bin && \
  chmod +x tizenstudio_installer.bin

# Install Tizen Studio
RUN ./tizenstudio_installer.bin --accept-license /home/${T_USER}/tizen-studio && \
    ./tizen-studio/package-manager/package-manager-cli.bin extra --add -n "TV SDK 3.1.2 Extension" -f /home/${T_USER}/tv_sdk_3.1.2_extension.zip && \
    	# Uninstall bloatware:
 	./tizen-studio/package-manager/package-manager-cli.bin uninstall \
 	MOBILE-6.0-WebAppDevelopment-CLI \
 	WEARABLE-6.0-WebAppDevelopment-CLI \
 	MOBILE-5.5-WebAppDevelopment-CLI \
 	WEARABLE-5.5-WebAppDevelopment-CLI \
 	MOBILE-5.0-WebAppDevelopment-CLI \
 	WEARABLE-5.0-WebAppDevelopment-CLI \
 	MOBILE-4.0-WebAppDevelopment-CLI \
 	WEARABLE-4.0-WebAppDevelopment-CLI \
 	MOBILE-3.0-WebAppDevelopment-CLI \
 	WEARABLE-3.0-WebAppDevelopment-CLI \
 	MOBILE-2.4-WebAppDevelopment-CLI \
 	WEARABLE-2.3.2-WebAppDevelopment-CLI \
 	MOBILE-2.3.1-WebAppDevelopment-CLI \
 	WEARABLE-2.3.1-WebAppDevelopment-CLI \
 	MOBILE-2.3-WebAppDevelopment-CLI \
 	WEARABLE-2.3-WebAppDevelopment-CLI \
 	&& \
 	# Remove the installer file
    rm tizenstudio_installer.bin tv_sdk_3.1.2_extension.zip

RUN ./tizen-studio/package-manager/package-manager-cli.bin install --accept-license \
	-d official \
	-s Tizen_Studio_${T_VERSION} \
#	Certificate-Manager \
	cert-add-on \
#	WebIDE \
	TV-SAMSUNG-Public-WebAppDevelopment \
	TV-3.0-samsung-public-WebAppDevelopment

#ENV DISPLAY=:0

# Build with: docker build . -t tizen-studio
# Run with: docker run --rm -it -u developer --net=host --name tizen-studio tizen-studio
