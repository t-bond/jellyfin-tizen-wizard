#!/bin/sh

cd /src/jellyfin-web
npm ci --no-audit
chmod -R 777 .
cd /src/jellyfin-tizen
JELLYFIN_WEB_DIR=/src/jellyfin-web/dist yarn install
chmod -R 777 .