#!/bin/bash

AUTHOR_PASS=$1
DISTRIBUTOR_PASS=$2
IP=$3

cd jellyfin-tizen
~/tizen-studio/tools/ide/bin/tizen security-profiles add -A -a /src/certs/author.p12 -d /src/certs/distributor.p12 -n JellyfinCertificate -p ${AUTHOR_PASS} -dp ${DISTRIBUTOR_PASS}
sed -i "s|/src/certs/author\\.pwd|${AUTHOR_PASS}|g" ~/tizen-studio-data/profile/profiles.xml
sed -i "s|/src/certs/distributor\\.pwd|${DISTRIBUTOR_PASS}|g" ~/tizen-studio-data/profile/profiles.xml
~/tizen-studio/tools/ide/bin/tizen cli-config "profiles.path=/home/`id -un`/tizen-studio-data/profile/profiles.xml"
~/tizen-studio/tools/ide/bin/tizen build-web -e ".*" -e "gulpfile.js" -e README.md -e "node_modules/*" -e "package*.json" -e "yarn.lock"
~/tizen-studio/tools/ide/bin/tizen package -t wgt -s JellyfinCertificate -o . -- .buildResult
~/tizen-studio/tools/sdb connect $IP
#~/tizen-studio/tools/sdb push /**/device-profile.xml /home/developer # Permit install application
~/tizen-studio/tools/ide/bin/tizen install -n ./Jellyfin.wgt -t `~/tizen-studio/tools/sdb devices | sed '1d' | rev | cut -f1 | rev`
