#!/bin/sh

rm -rf jellyfin-web jellyfin-tizen
git clone --depth 1 https://github.com/jellyfin/jellyfin-web.git
git clone --depth 1 https://github.com/jellyfin/jellyfin-tizen.git
